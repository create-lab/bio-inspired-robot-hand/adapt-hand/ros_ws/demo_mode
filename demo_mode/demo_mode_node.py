import rclpy
from rclpy.node import Node
from std_msgs.msg import Float64MultiArray
from sensor_msgs.msg import Joy
from helper_functions.gamepad_functions import GamepadFunctions

class DemoMode(Node):

    def __init__(self):
        super().__init__('demo_mode')
        
        self.gp = GamepadFunctions()

        # Publisher
        self.tendon_pos_publisher = self.create_publisher(Float64MultiArray, '/tendon_position_demand', 10)
        
        # Subscriber
        self.true_tendon_position_subscriber_ = self.create_subscription(Float64MultiArray, '/hand_controller/tendon_positions', 
                                                        self.true_tendon_position_callback, 10)
        
        self.mixer_tendon_demand_subscriber_ = self.create_subscription(Float64MultiArray, '/mixer_tendon_position_demand', 
                                                        self.mixer_tendon_input_callback, 10)

        self.glove_tendon_demand_subscriber_ = self.create_subscription(Float64MultiArray, '/glove_tendon_position_demand', 
                                                        self.glove_tendon_input_callback, 10)
        
        self.mainloop = self.create_timer(0.001, self.mainloop_callback) 
        self.gamepad_subscriber = self.create_subscription(Joy, '/gamepad', self.gamepad_callback, 10)

        self.glove_tendon_demand = None
        self.mixer_tendon_demand = None
        self.true_tendon_position = None

        self.is_demo_enable = False

        self.use_mixer_signal = True

        self.deadman_switch_pressed = False

        self.raw_gp_buttons = None

        self.manual_to_zero = False

        print("\n=================")
        
        print("Demo mode!")
        print("Press CIRCLE / CROSS for activate/deactivate")
        print("Press TRIANGLE / SQUARE for mixer/glove")
        
        print("=================\n")


    def is_target_present(self):
        if self.manual_to_zero:
            return True
        
        if self.true_tendon_position is None:
            return False
        
        if self.is_demo_enable == False:
            return False
        
        if self.use_mixer_signal:
            if self.mixer_tendon_demand is None:
                return False
        else:
            if (self.glove_tendon_demand is None) or (self.deadman_switch_pressed == False):
                return False
            
        return True

    def true_tendon_position_callback(self, msg: Float64MultiArray):
        self.true_tendon_position = msg.data

    def mixer_tendon_input_callback(self, msg: Float64MultiArray):
        self.mixer_tendon_demand = msg.data

    def glove_tendon_input_callback(self, msg: Float64MultiArray):
        self.glove_tendon_demand = msg.data
        # self.glove_tendon_demand = [0] *12
        # try:
        #     for i in range(12):
        #         self.glove_tendon_demand[i] = msg.data[i]
        # except:
        #     pass
        # print(msg.data[0])

    def gamepad_callback(self, gamepad_raw_msg:Joy):
        self.gp.convert_joy_msg_to_dictionary(gamepad_raw_msg)

    def mainloop_callback(self):
        if self.gp.if_button_pressed("triangle"):
            self.use_mixer_signal = True
            print("\n=================");print("Operating mode set to: MIXER");print("=================\n")
        
        if self.gp.if_button_pressed("square"):
            self.use_mixer_signal = False
            print("\n=================");print("Operating mode set to: GLOVE");print("=================\n")

        if self.gp.if_button_pressed("circle"):
            self.is_demo_enable = True
            print("\n=================");print("DEMO -- ACTIVATE");print("=================\n")

        if self.gp.if_button_pressed("cross"):
            self.is_demo_enable = False
            print("\n=================");print("DEMO -- DEACTIVATE");print("=================\n")


        if self.gp.button_data["L1"] == 1:
            self.deadman_switch_pressed = True
        else:
            self.deadman_switch_pressed = False

        if self.gp.button_data["R1"] == 1:
            self.manual_to_zero = True
        else:
            self.manual_to_zero = False

        if self.is_target_present():
            # Throttle tendon demand
            
            # print(self.glove_tendon_demand)
            if self.use_mixer_signal:
                # print(4)
                target_signal = self.mixer_tendon_demand
                decay = 0.9
            else:
                target_signal = self.glove_tendon_demand
                decay = 0.9
                # print(3)

            # print(self.glove_tendon_demand)
            if self.manual_to_zero:
                target_signal = [0.0] * 12
                decay = 0.95

            demand_signal = []
            for current_pos, target in zip(self.true_tendon_position, target_signal):
                demand_signal.append(current_pos*decay + target*(1-decay))

            tendon_pos_demand = Float64MultiArray()
            tendon_pos_demand.data = demand_signal
            self.tendon_pos_publisher.publish(tendon_pos_demand)


def main(args=None):
    rclpy.init(args=args)

    demo_mode = DemoMode()

    rclpy.spin(demo_mode)

    demo_mode.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()